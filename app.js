var $video = $( 'video' );
var video = $video[0];

$video.on( 'touchstart pointerover mouseover pointerdown mousedown', function () {
	if ( video.playing ) {
		video.pause();
	} else {
		video.play();
	}

	$( '.swipe-target' ).show();
});

$video.on( 'play', function () {
	var running = true;

	function loop () {
		if ( !running ) return;

		requestAnimationFrame( loop );

		if ( video.currentTime > 3.6 ) {
			video.currentTime = 0;
		}
	}

	loop();

	$video.one('pause', function () {
		running = false;
	});
});

var scale = linearScale([ 0, 1 ], [ 3.7, 5.8 ]);

function look(event) {
		event.preventDefault();

		var x = event.clientX || event.touches[0].clientX;
		var p = x / window.innerWidth;

		var s = scale( p );

		video.pause();
		video.currentTime = s;
}


$( '.swipe-target' ).on( 'touchstart pointerover mouseover pointerdown mousedown', function ( event ) {
	event.preventDefault();

	$( window ).on( 'touchmove pointermove pointerover mouseover mousemove', look);

	$( window ).on( 'touchend mouseout pointerout mouseup pointerup', function () {
		video.play();
		$( window ).off( 'touchmove pointermove pointerover mouseover mousemove', look);
	});
});
